-module(eval).

-export([print/1]).

-type expr() :: {num, integer()} | {var, atom()} |
		{add, expr(), expr()} | {mul, expr(), expr()}.

%% Turn an expression into a string.
-spec print(expr()) -> string.

print({num, N}) -> integer_to_list(N);
print({var, A}) -> atom_to_list(A);
print({add, E1, E2}) ->
    "(" ++ print(E1) ++ "+" ++ print(E2) ++ ")";
print({mul, E1, E2}) ->
    "(" ++ print(E1) ++ "*" ++ print(E2) ++ ")".

-type env() :: [{atom(), integer()}].

%% Turn an expression into a number, by working out its value.
-spec eval(env(), expr()) -> integer().

eval(_Env, {num, N}) -> N;
eval(Env, {var, A}) -> lookup(A, Env);
eval(Env, {add, E1, E2}) ->
    eval(Env, E1) + eval(Env, E2);
eval(Env, {mul, E1, E2}) ->
    eval(Env, E1) * eval(Env, E2).

-spec lookup(atom, env) -> integer.

lookup(A, [{A, V} | _]) -> V;
lookup(A, [_ | Rest]) -> lookup(A, Rest).
